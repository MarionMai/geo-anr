# Geography of research projects funded by the French National Agency of Research

Studying the geography of ANR projects (research contracts funded by the french National Research Agency).
Processing open data produced and delivered by the French National Research Agence (ANR):
ULR : [https://www.data.gouv.fr/fr/datasets/projets-anr-dos-depuis-2010-partenaires/](https://www.data.gouv.fr/fr/datasets/projets-anr-dos-depuis-2010-partenaires/)

# Geographie des projets de recherche financés par l'Agence Nationale de la Recherche

Au 7 juin 2021, ce projet contient une première analyse exploratoire de la base de données des Projets et Partenaires des projets financés depuis 2010 par la Direction des opérations scientifiques (DOS) de l'ANR. La base de données a été mise à jour pour la dernière fois en avril 2021.

L'analyse réalisée avec R est consultable au lien suivant : [https://marionmai.frama.io/geo-anr](https://marionmai.frama.io/geo-anr).

Cette analyse a fait l'objet d'une première présentation lors de la journée d'étude "L'argent de la science" organisée par l'Opération Mondes Scientifiques du labex SMS. Le support de présentation est accessible dans le dossier : [presentation-labexsms](https://framagit.org/MarionMai/geo-anr/-/tree/main/presentation-labexsms)

Le script, les données et figures sont dans le dossier [ANR](https://framagit.org/MarionMai/geo-anr/-/tree/main/ANR). 



